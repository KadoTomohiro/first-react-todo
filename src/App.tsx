import {Provider} from "mobx-react";
import * as React from 'react';
import './App.css';
import List from "./components/List";
import {TodoStore} from "./stores/todo.store";
// import {CalculateStore} from "./stores/calculate.store";

// const calc = new CalculateStore();

const todoStore = new TodoStore();

class App extends React.Component {
  public render() {
    return (
      <div className="App">
          <Provider todoStore={todoStore}>
              <List/>
          </Provider>
      </div>
    );
  }
}

export default App;
