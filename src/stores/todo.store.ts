import {action, observable} from "mobx";
import {ITodo} from "../Models/Todo.model";

export interface ITodoStoreType {
    list: ITodo[];
    toggleFinished: (index: number) => void
}

export class TodoStore implements ITodoStoreType{
    @observable
    public list: ITodo[] = [
        {task: 'メール', finished: false},
        {task: 'ミーティング', finished: false},
    ];

    @action.bound
    public toggleFinished(index: number): void {
        const todo = this.list[index];
        this.list[index] = {
            finished: !todo.finished,
            task: todo.task
        }
    }
}