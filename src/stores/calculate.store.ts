import {action, computed, observable} from "mobx";

export interface ICalculateStoreType {
    num: number;
    double: number;
    multiple: (multiplier: number) => number;
    increment: () => void;
    decrement: () => void;
}

export class CalculateStore implements ICalculateStoreType{
    @observable
    public num = 0;

    @computed
    public get double(): number {
        return this.num * 2;
    }

    public multiple(multiplier: number) {
        return computed(() => this.num * multiplier).get();
    }


    @action.bound
    public decrement(): void {
        this.num--;
    }

    @action.bound
    public increment() {
        this.num++;
    }
}