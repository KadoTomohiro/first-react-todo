import {observer} from "mobx-react";
import * as React from "react";
import {ITodo} from "../Models/Todo.model";

interface IItemPropType {
    todo: ITodo;
    onChange?: () => void
}

@observer
class Item extends React.Component<IItemPropType> {
    public render() {
        const { todo, onChange } = this.props;
        return (
            <React.Fragment>
                <input type="checkbox" checked={todo.finished} readOnly={true} onClick={onChange}/>{todo.task}
            </React.Fragment>
        )
    }
}

export default Item;