import {inject, observer} from "mobx-react";
import * as React from "react";
import {ReactNode} from "react";
import {ITodo} from "../Models/Todo.model";
import {ITodoStoreType} from "../stores/todo.store";
import Item from "./Item";

interface IListPropsType {
    todoStore?: ITodoStoreType;
}

@inject('todoStore')
@observer
class List extends React.Component<IListPropsType> {
    public render() {
        const {todoStore} = this.props;
        return (
            <ul>
                {this.getListItem(todoStore!.list)}
            </ul>
        )
    }

    private getListItem(list: ITodo[]): ReactNode[] {
        return list
            .map((todo, index) => {
                return (
                    <li key={index}>
                        <Item todo={todo} onChange={this.changeHandlerFactory(index)}/>
                    </li>
                )
            });
    }

    private changeHandlerFactory(index: number): () => void {
        const {todoStore} = this.props;
        return () => {
                if (todoStore) {todoStore.toggleFinished(index)}
            };
    };
}

export default List;