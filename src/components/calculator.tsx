import {inject, observer} from "mobx-react";
import * as React from "react";
import {ICalculateStoreType} from "../stores/calculate.store";
import Multiple from "./Multiple";

interface ICalculatePropType {
    calc?: ICalculateStoreType;
}

@inject('calc')
@observer
class Calculator extends React.Component<ICalculatePropType> {
    public render() {
        const { calc } = this.props;
        return (
            <React.Fragment>
                <button onClick={calc!.decrement}>-</button>
                <span>{calc!.num}</span>
                <button onClick={calc!.increment}>+</button>
                <p><Multiple multiplier={2}/></p>
                <p><Multiple multiplier={3}/></p>
                <p><Multiple multiplier={4}/></p>
                <p><Multiple multiplier={5}/></p>
                <p><Multiple multiplier={6}/></p>
            </React.Fragment>
        );
    }
}

export default Calculator;