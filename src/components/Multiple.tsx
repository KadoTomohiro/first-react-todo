import {inject} from "mobx-react";
import * as React from "react";
import {ICalculateStoreType} from "../stores/calculate.store";

interface IMultiplePropsType {
    multiplier: number;
    calc?: ICalculateStoreType
}

@inject('calc')
class Multiple extends React.Component<IMultiplePropsType>{
    public render() {
        const { calc } = this.props;
        return (
            <React.Fragment>
                {calc!.multiple(this.props.multiplier)}
            </React.Fragment>
        );
    }
}

export default Multiple;