export interface ITodo {
    task: string;
    finished: boolean;
}